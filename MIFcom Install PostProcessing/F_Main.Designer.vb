﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class F_Main
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(F_Main))
        Me.B_Cleanup = New System.Windows.Forms.Button()
        Me.B_Backup = New System.Windows.Forms.Button()
        Me.B_Shutdown = New System.Windows.Forms.Button()
        Me.B_ExitAudit = New System.Windows.Forms.Button()
        Me.CB_Cleanup = New System.Windows.Forms.CheckBox()
        Me.CB_ExitAudit = New System.Windows.Forms.CheckBox()
        Me.CB_Backup = New System.Windows.Forms.CheckBox()
        Me.CB_Shutdown = New System.Windows.Forms.CheckBox()
        Me.B_Start = New System.Windows.Forms.Button()
        Me.B_UACActivate = New System.Windows.Forms.Button()
        Me.CB_CopyUSB = New System.Windows.Forms.CheckBox()
        Me.B_CopyUSB = New System.Windows.Forms.Button()
        Me.TLP_Main = New System.Windows.Forms.TableLayoutPanel()
        Me.TableLayoutPanel2 = New System.Windows.Forms.TableLayoutPanel()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.CB_Drive = New System.Windows.Forms.ComboBox()
        Me.CB_UACActivate = New System.Windows.Forms.CheckBox()
        Me.CB_ActivateWindows = New System.Windows.Forms.CheckBox()
        Me.B_ActivateWindows = New System.Windows.Forms.Button()
        Me.L_DPK = New System.Windows.Forms.Label()
        Me.B_HoloGate = New System.Windows.Forms.Button()
        Me.CB_HoloGate = New System.Windows.Forms.CheckBox()
        Me.TLP_Main.SuspendLayout()
        Me.TableLayoutPanel2.SuspendLayout()
        Me.SuspendLayout()
        '
        'B_Cleanup
        '
        Me.B_Cleanup.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.B_Cleanup.Location = New System.Drawing.Point(18, 144)
        Me.B_Cleanup.Name = "B_Cleanup"
        Me.B_Cleanup.Size = New System.Drawing.Size(238, 35)
        Me.B_Cleanup.TabIndex = 0
        Me.B_Cleanup.Text = "Bereinigen"
        Me.B_Cleanup.UseVisualStyleBackColor = True
        '
        'B_Backup
        '
        Me.B_Backup.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.B_Backup.Location = New System.Drawing.Point(20, 288)
        Me.B_Backup.Name = "B_Backup"
        Me.B_Backup.Size = New System.Drawing.Size(235, 35)
        Me.B_Backup.TabIndex = 1
        Me.B_Backup.Text = "Backup"
        Me.B_Backup.UseVisualStyleBackColor = True
        '
        'B_Shutdown
        '
        Me.B_Shutdown.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.B_Shutdown.Location = New System.Drawing.Point(20, 384)
        Me.B_Shutdown.Name = "B_Shutdown"
        Me.B_Shutdown.Size = New System.Drawing.Size(235, 35)
        Me.B_Shutdown.TabIndex = 2
        Me.B_Shutdown.Text = "Löschen und Herunterfahren"
        Me.B_Shutdown.UseVisualStyleBackColor = True
        '
        'B_ExitAudit
        '
        Me.B_ExitAudit.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.B_ExitAudit.Location = New System.Drawing.Point(20, 240)
        Me.B_ExitAudit.Name = "B_ExitAudit"
        Me.B_ExitAudit.Size = New System.Drawing.Size(235, 35)
        Me.B_ExitAudit.TabIndex = 3
        Me.B_ExitAudit.Text = "Audit beenden"
        Me.B_ExitAudit.UseVisualStyleBackColor = True
        '
        'CB_Cleanup
        '
        Me.CB_Cleanup.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.CB_Cleanup.AutoSize = True
        Me.CB_Cleanup.Checked = True
        Me.CB_Cleanup.CheckState = System.Windows.Forms.CheckState.Checked
        Me.CB_Cleanup.Location = New System.Drawing.Point(355, 155)
        Me.CB_Cleanup.Name = "CB_Cleanup"
        Me.CB_Cleanup.Size = New System.Drawing.Size(15, 14)
        Me.CB_Cleanup.TabIndex = 4
        Me.CB_Cleanup.UseVisualStyleBackColor = True
        '
        'CB_ExitAudit
        '
        Me.CB_ExitAudit.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.CB_ExitAudit.AutoSize = True
        Me.CB_ExitAudit.Checked = True
        Me.CB_ExitAudit.CheckState = System.Windows.Forms.CheckState.Checked
        Me.CB_ExitAudit.Location = New System.Drawing.Point(355, 251)
        Me.CB_ExitAudit.Name = "CB_ExitAudit"
        Me.CB_ExitAudit.Size = New System.Drawing.Size(15, 14)
        Me.CB_ExitAudit.TabIndex = 5
        Me.CB_ExitAudit.UseVisualStyleBackColor = True
        '
        'CB_Backup
        '
        Me.CB_Backup.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.CB_Backup.AutoSize = True
        Me.CB_Backup.Checked = True
        Me.CB_Backup.CheckState = System.Windows.Forms.CheckState.Checked
        Me.CB_Backup.Location = New System.Drawing.Point(355, 299)
        Me.CB_Backup.Name = "CB_Backup"
        Me.CB_Backup.Size = New System.Drawing.Size(15, 14)
        Me.CB_Backup.TabIndex = 6
        Me.CB_Backup.UseVisualStyleBackColor = True
        '
        'CB_Shutdown
        '
        Me.CB_Shutdown.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.CB_Shutdown.AutoSize = True
        Me.CB_Shutdown.Checked = True
        Me.CB_Shutdown.CheckState = System.Windows.Forms.CheckState.Checked
        Me.CB_Shutdown.Location = New System.Drawing.Point(355, 395)
        Me.CB_Shutdown.Name = "CB_Shutdown"
        Me.CB_Shutdown.Size = New System.Drawing.Size(15, 14)
        Me.CB_Shutdown.TabIndex = 7
        Me.CB_Shutdown.UseVisualStyleBackColor = True
        '
        'B_Start
        '
        Me.B_Start.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.TLP_Main.SetColumnSpan(Me.B_Start, 2)
        Me.B_Start.Location = New System.Drawing.Point(12, 481)
        Me.B_Start.Name = "B_Start"
        Me.B_Start.Size = New System.Drawing.Size(427, 42)
        Me.B_Start.TabIndex = 8
        Me.B_Start.Text = "Alle starten"
        Me.B_Start.UseVisualStyleBackColor = True
        '
        'B_UACActivate
        '
        Me.B_UACActivate.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.B_UACActivate.Location = New System.Drawing.Point(19, 96)
        Me.B_UACActivate.Name = "B_UACActivate"
        Me.B_UACActivate.Size = New System.Drawing.Size(237, 35)
        Me.B_UACActivate.TabIndex = 11
        Me.B_UACActivate.Text = "UAC im Registry Aktivieren"
        Me.B_UACActivate.UseVisualStyleBackColor = True
        '
        'CB_CopyUSB
        '
        Me.CB_CopyUSB.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.CB_CopyUSB.AutoSize = True
        Me.CB_CopyUSB.Checked = True
        Me.CB_CopyUSB.CheckState = System.Windows.Forms.CheckState.Checked
        Me.CB_CopyUSB.Location = New System.Drawing.Point(355, 203)
        Me.CB_CopyUSB.Name = "CB_CopyUSB"
        Me.CB_CopyUSB.Size = New System.Drawing.Size(15, 14)
        Me.CB_CopyUSB.TabIndex = 13
        Me.CB_CopyUSB.UseVisualStyleBackColor = True
        '
        'B_CopyUSB
        '
        Me.B_CopyUSB.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.B_CopyUSB.Location = New System.Drawing.Point(18, 192)
        Me.B_CopyUSB.Name = "B_CopyUSB"
        Me.B_CopyUSB.Size = New System.Drawing.Size(238, 35)
        Me.B_CopyUSB.TabIndex = 12
        Me.B_CopyUSB.Text = "USB kopieren"
        Me.B_CopyUSB.UseVisualStyleBackColor = True
        '
        'TLP_Main
        '
        Me.TLP_Main.ColumnCount = 2
        Me.TLP_Main.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 61.19734!))
        Me.TLP_Main.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 38.80266!))
        Me.TLP_Main.Controls.Add(Me.CB_CopyUSB, 1, 4)
        Me.TLP_Main.Controls.Add(Me.CB_Backup, 1, 6)
        Me.TLP_Main.Controls.Add(Me.CB_ExitAudit, 1, 5)
        Me.TLP_Main.Controls.Add(Me.B_Start, 0, 10)
        Me.TLP_Main.Controls.Add(Me.B_Cleanup, 0, 3)
        Me.TLP_Main.Controls.Add(Me.B_CopyUSB, 0, 4)
        Me.TLP_Main.Controls.Add(Me.CB_Cleanup, 1, 3)
        Me.TLP_Main.Controls.Add(Me.B_ExitAudit, 0, 5)
        Me.TLP_Main.Controls.Add(Me.B_Backup, 0, 6)
        Me.TLP_Main.Controls.Add(Me.TableLayoutPanel2, 0, 7)
        Me.TLP_Main.Controls.Add(Me.B_Shutdown, 0, 8)
        Me.TLP_Main.Controls.Add(Me.CB_Shutdown, 1, 8)
        Me.TLP_Main.Controls.Add(Me.B_UACActivate, 0, 2)
        Me.TLP_Main.Controls.Add(Me.CB_UACActivate, 1, 2)
        Me.TLP_Main.Controls.Add(Me.CB_ActivateWindows, 1, 1)
        Me.TLP_Main.Controls.Add(Me.B_ActivateWindows, 0, 1)
        Me.TLP_Main.Controls.Add(Me.L_DPK, 0, 0)
        Me.TLP_Main.Controls.Add(Me.B_HoloGate, 0, 9)
        Me.TLP_Main.Controls.Add(Me.CB_HoloGate, 1, 9)
        Me.TLP_Main.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TLP_Main.GrowStyle = System.Windows.Forms.TableLayoutPanelGrowStyle.FixedSize
        Me.TLP_Main.Location = New System.Drawing.Point(0, 0)
        Me.TLP_Main.Name = "TLP_Main"
        Me.TLP_Main.RowCount = 11
        Me.TLP_Main.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.091074!))
        Me.TLP_Main.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 42.0!))
        Me.TLP_Main.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.090166!))
        Me.TLP_Main.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.091075!))
        Me.TLP_Main.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.091075!))
        Me.TLP_Main.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.091075!))
        Me.TLP_Main.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.091075!))
        Me.TLP_Main.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.091075!))
        Me.TLP_Main.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.091075!))
        Me.TLP_Main.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.091075!))
        Me.TLP_Main.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.091075!))
        Me.TLP_Main.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TLP_Main.Size = New System.Drawing.Size(451, 530)
        Me.TLP_Main.TabIndex = 14
        '
        'TableLayoutPanel2
        '
        Me.TableLayoutPanel2.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.TableLayoutPanel2.ColumnCount = 2
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 32.35294!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 67.64706!))
        Me.TableLayoutPanel2.Controls.Add(Me.Label1, 0, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.CB_Drive, 1, 0)
        Me.TableLayoutPanel2.Location = New System.Drawing.Point(24, 336)
        Me.TableLayoutPanel2.Name = "TableLayoutPanel2"
        Me.TableLayoutPanel2.RowCount = 1
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel2.Size = New System.Drawing.Size(227, 35)
        Me.TableLayoutPanel2.TabIndex = 15
        '
        'Label1
        '
        Me.Label1.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(3, 11)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(67, 13)
        Me.Label1.TabIndex = 10
        Me.Label1.Text = "Backup Ziel:"
        '
        'CB_Drive
        '
        Me.CB_Drive.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.CB_Drive.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CB_Drive.FormattingEnabled = True
        Me.CB_Drive.Location = New System.Drawing.Point(76, 7)
        Me.CB_Drive.Name = "CB_Drive"
        Me.CB_Drive.Size = New System.Drawing.Size(148, 21)
        Me.CB_Drive.TabIndex = 9
        '
        'CB_UACActivate
        '
        Me.CB_UACActivate.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.CB_UACActivate.AutoSize = True
        Me.CB_UACActivate.Checked = True
        Me.CB_UACActivate.CheckState = System.Windows.Forms.CheckState.Checked
        Me.CB_UACActivate.Location = New System.Drawing.Point(355, 107)
        Me.CB_UACActivate.Name = "CB_UACActivate"
        Me.CB_UACActivate.Size = New System.Drawing.Size(15, 14)
        Me.CB_UACActivate.TabIndex = 20
        Me.CB_UACActivate.UseVisualStyleBackColor = True
        '
        'CB_ActivateWindows
        '
        Me.CB_ActivateWindows.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.CB_ActivateWindows.AutoSize = True
        Me.CB_ActivateWindows.Checked = True
        Me.CB_ActivateWindows.CheckState = System.Windows.Forms.CheckState.Checked
        Me.CB_ActivateWindows.Location = New System.Drawing.Point(355, 62)
        Me.CB_ActivateWindows.Name = "CB_ActivateWindows"
        Me.CB_ActivateWindows.Size = New System.Drawing.Size(15, 14)
        Me.CB_ActivateWindows.TabIndex = 18
        Me.CB_ActivateWindows.UseVisualStyleBackColor = True
        '
        'B_ActivateWindows
        '
        Me.B_ActivateWindows.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.B_ActivateWindows.Location = New System.Drawing.Point(20, 51)
        Me.B_ActivateWindows.Name = "B_ActivateWindows"
        Me.B_ActivateWindows.Size = New System.Drawing.Size(235, 35)
        Me.B_ActivateWindows.TabIndex = 19
        Me.B_ActivateWindows.Text = "Windows aktivieren"
        Me.B_ActivateWindows.UseVisualStyleBackColor = True
        '
        'L_DPK
        '
        Me.L_DPK.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.L_DPK.AutoSize = True
        Me.L_DPK.Location = New System.Drawing.Point(123, 17)
        Me.L_DPK.Name = "L_DPK"
        Me.L_DPK.Size = New System.Drawing.Size(29, 13)
        Me.L_DPK.TabIndex = 21
        Me.L_DPK.Text = "DPK"
        '
        'B_HoloGate
        '
        Me.B_HoloGate.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.B_HoloGate.BackgroundImage = CType(resources.GetObject("B_HoloGate.BackgroundImage"), System.Drawing.Image)
        Me.B_HoloGate.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.B_HoloGate.Location = New System.Drawing.Point(20, 432)
        Me.B_HoloGate.Name = "B_HoloGate"
        Me.B_HoloGate.Size = New System.Drawing.Size(235, 35)
        Me.B_HoloGate.TabIndex = 23
        Me.B_HoloGate.UseVisualStyleBackColor = True
        Me.B_HoloGate.Visible = False
        '
        'CB_HoloGate
        '
        Me.CB_HoloGate.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.CB_HoloGate.AutoSize = True
        Me.CB_HoloGate.Checked = True
        Me.CB_HoloGate.CheckState = System.Windows.Forms.CheckState.Checked
        Me.CB_HoloGate.Location = New System.Drawing.Point(355, 443)
        Me.CB_HoloGate.Name = "CB_HoloGate"
        Me.CB_HoloGate.Size = New System.Drawing.Size(15, 14)
        Me.CB_HoloGate.TabIndex = 24
        Me.CB_HoloGate.UseVisualStyleBackColor = True
        Me.CB_HoloGate.Visible = False
        '
        'F_Main
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(451, 530)
        Me.Controls.Add(Me.TLP_Main)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "F_Main"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "MIPP"
        Me.TLP_Main.ResumeLayout(False)
        Me.TLP_Main.PerformLayout()
        Me.TableLayoutPanel2.ResumeLayout(False)
        Me.TableLayoutPanel2.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents B_Cleanup As System.Windows.Forms.Button
    Friend WithEvents B_Backup As System.Windows.Forms.Button
    Friend WithEvents B_Shutdown As System.Windows.Forms.Button
    Friend WithEvents B_ExitAudit As System.Windows.Forms.Button
    Friend WithEvents CB_Cleanup As System.Windows.Forms.CheckBox
    Friend WithEvents CB_ExitAudit As System.Windows.Forms.CheckBox
    Friend WithEvents CB_Backup As System.Windows.Forms.CheckBox
    Friend WithEvents CB_Shutdown As System.Windows.Forms.CheckBox
    Friend WithEvents B_Start As System.Windows.Forms.Button
    Friend WithEvents B_UACActivate As System.Windows.Forms.Button
    Friend WithEvents CB_CopyUSB As System.Windows.Forms.CheckBox
    Friend WithEvents B_CopyUSB As System.Windows.Forms.Button
    Friend WithEvents TLP_Main As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents CB_ActivateWindows As System.Windows.Forms.CheckBox
    Friend WithEvents B_ActivateWindows As System.Windows.Forms.Button
    Friend WithEvents CB_UACActivate As System.Windows.Forms.CheckBox
    Friend WithEvents L_DPK As System.Windows.Forms.Label
    Friend WithEvents TableLayoutPanel2 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents CB_Drive As System.Windows.Forms.ComboBox
    Friend WithEvents B_HoloGate As Button
    Friend WithEvents CB_HoloGate As CheckBox
End Class
