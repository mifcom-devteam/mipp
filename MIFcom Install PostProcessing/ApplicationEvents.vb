﻿Imports CommonClasses

Namespace My

    ' The following events are available for MyApplication:
    ' 
    ' Startup: Raised when the application starts, before the startup form is created.
    ' Shutdown: Raised after all application forms are closed.  This event is not raised if the application terminates abnormally.
    ' UnhandledException: Raised if the application encounters an unhandled exception.
    ' StartupNextInstance: Raised when launching a single-instance application and the application is already active. 
    ' NetworkAvailabilityChanged: Raised when the network connection is connected or disconnected.
    Partial Friend Class MyApplication
        Sub AppStart(sender As Object, e As ApplicationServices.StartupEventArgs) Handles Me.Startup
            Try
                TransactionController.SQLConnector = (New SQLAuthorizator.AuthorizedConnector(SQLAuthorizator.AuthorizationClient.I.GetCredentials("MIFCOMTOOLDB")))
            Catch ex As Exception
                Try
                    SendErrorMailToDeveloper(ex, "Auth error.")
                Catch ex2 As Exception
                    ErrBox.Go(ex2)
                End Try
                ErrBox.Go(ex, "Tool befindet sich nicht im Netzwerk und beendet.")
                e.Cancel = True
            End Try
        End Sub
    End Class


End Namespace

