﻿Public Class LogDialog

    Shared Property InfoText As String
    Shared Property InfoTextChanged As Boolean
    Shared Property FormStatus As FormStatEnum = FormStatEnum.Closed

    Enum FormStatEnum
        Initialising = 0
        Running = 1
        Canceled = 2
        Closing = 3
        Closed = 4
    End Enum


    Private Sub New()
        InitializeComponent()
        FormStatus = FormStatEnum.Running
        timerx.Interval = 100
        timerx.Start()
    End Sub

    Dim WithEvents timerx As New Timer

    Shared Property Dialog As LogDialog

    Private Sub runnnn() Handles timerx.Tick

        If InfoTextChanged = True Then
            Me.RTB_Log.Text = InfoText
            SetLocation()
            InfoTextChanged = False
        End If

        Select Case FormStatus
            Case FormStatEnum.Running

            Case FormStatEnum.Canceled

            Case FormStatEnum.Closing
                Me.Close()
            Case FormStatEnum.Closed
                Me.Close()
        End Select

    End Sub

    Private Shared Sub Init(val As String)
        InfoText = val
    End Sub

    Public Shared Sub Go(val As String)
        Init(val)
        RunGo()
    End Sub

    Private Shared Sub RunGo()
        FormStatus = FormStatEnum.Initialising
        Dim t As New Threading.Thread(AddressOf Generate)
        t.Name = "ProcessingDialog"
        t.Start()
    End Sub

    Private Shared Sub Generate()
        Dim f As New LogDialog()
        Dialog = f
        Application.Run(f)
    End Sub

    Public Shared Sub SetText(ByVal val As String)
        InfoText = val
        InfoTextChanged = True
    End Sub

    Private Sub B_Cancel_Click(sender As System.Object, e As System.EventArgs)
        FormStatus = FormStatEnum.Canceled
    End Sub

    Public Shared Sub CloseForm()
        FormStatus = FormStatEnum.Closing
    End Sub

    Private Sub ProcessingDialog_FormClosed(sender As System.Object, e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        FormStatus = FormStatEnum.Closed
    End Sub

    Private Sub LogDialog_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        SetLocation()
    End Sub

    Dim w As Window = Nothing
    Private Sub SetLocation()
        'Me.Location = New Point(F_Main., F_Main.DesktopLocation.Y)

        If Not w Is Nothing Then
            Me.Left = w.Location.X
            Me.Top = w.Location.Y + w.Bounds.Height
        Else
            w = Window.Find("MIPP")
        End If

    End Sub
End Class
