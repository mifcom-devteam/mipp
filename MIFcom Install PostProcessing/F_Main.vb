﻿Imports System.IO
Imports Microsoft.Win32
Imports System.Data.SqlClient
Imports CommonClasses
Imports SharedUtils


Public Class F_Main

    Property LogString As String = ""
    Property User As User
    Property OrderID As String
    Property OrderPCID As Integer
    Property Order As CustomerOrderOrOffer = Nothing
    Property MakeBackup2USB As Boolean = False
    Property PC As PCEntity
    Property IsHoloGate As Boolean = False

    Private Sub F_Main_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            CacheController.ReloadAll()

            Me.Text = "MIPP  V" & Version.MAJOR & "." & Version.MINOR
            Me.Location = New Point(Me.Location.X, Me.Location.Y - 200)

            WCR(Sub(tran)
                    CompanyColl.I.Fill()
                    ProductionEvent.EventTypesColl.I.Fill()
                    LoadSettings(tran)
                End Sub)


            LoadOrderIDAndUser()

            Order = WCR(Function(tran)
                            Dim ordF As New CustomerOrder.Factory
                            Return ordF.GetOrderByID(OrderID, tran)
                        End Function)
            If Order Is Nothing Then
                Throw New Exception("Auftrag ist leer!")
            End If

            Dim PCs = WCR(Function(tran)
                              Return Order.GetPCs(tran)
                          End Function)
            PC = PCs.Where(Function(x) x.PCID = OrderPCID).FirstOrDefault

            If PC IsNot Nothing Then
                For Each pos In PC.GetPositionList().ToList
                    If pos.Description.ToUpper.Contains("HOLOGATE") Then
                        IsHoloGate = True
                    End If
                Next
            Else
                Throw New Exception("PC is nothing!")
            End If

            Dim Positions = PC.GetPositionList(True).ToList

            For Each pos In Positions
                If pos.WI.ItemDBID = 9924 Then
                    MakeBackup2USB = True
                End If
            Next


            If User Is Nothing OrElse User.UserID = 0 Then
                TLP_Main.Enabled = False
                MsgBox("Es wurde kein Installateur hinterlegt.")
                Exit Sub
            Else
                'TestFinished()
            End If

            Dim foundZDrive As Boolean = False
            For Each drive As DriveInfo In DriveInfo.GetDrives
                If drive.DriveType = DriveType.Fixed AndAlso Not drive.RootDirectory.FullName = "C:\" Then
                    CB_Drive.Items.Add(drive.RootDirectory)
                End If
            Next
            If Not MakeBackup2USB Then
                If CB_Drive.Items.Count > 0 Then
                    CB_Drive.SelectedIndex = CB_Drive.Items.Count - 1
                Else
                    B_Backup.BackColor = Color.Red
                    CB_Drive.Enabled = False
                    CB_Backup.Checked = False
                    CB_Backup.Enabled = False
                    B_Backup.Enabled = False
                End If
            Else
                CB_Drive.Enabled = False
                B_Backup.Text = "A+ Installation Backup auf USB"
            End If




            Dim w As Window = Window.Find("Systemvorbereitungsprogramm 3.14", True)
            If Not w Is Nothing Then
                MsgBox("Sysprep - Fenster ist offen, bitte schliesen.")
            End If

            If (My.Computer.Info.OSFullName & " " & getWMIInfo("OSArchitecture", "Win32_OperatingSystem")).Contains("Windows 7") Then
                CB_ActivateWindows.Checked = False
                CB_ActivateWindows.Enabled = False
                B_ActivateWindows.Enabled = False
                L_DPK.Text = ""
            Else
                If GetDPKKey() = "" Then
                    L_DPK.Text = "Kein Windows Key im Bios gefunden."
                    L_DPK.ForeColor = Color.Red
                    CB_ActivateWindows.Checked = False
                    CB_ActivateWindows.Enabled = False
                    B_ActivateWindows.Enabled = False
                Else
                    L_DPK.ForeColor = Color.Green
                    L_DPK.Text = GetDPKKey()
                    CB_ActivateWindows.Checked = True
                    'CB_ActivateWindows.Enabled = False
                End If
            End If

            If IsHoloGate Then
                B_UACActivate.Enabled = False
                CB_UACActivate.Checked = False
                CB_UACActivate.Enabled = False

                B_CopyUSB.Enabled = False
                CB_CopyUSB.Checked = False
                CB_CopyUSB.Enabled = False

                B_ExitAudit.Enabled = False
                CB_ExitAudit.Checked = False
                CB_ExitAudit.Enabled = False

                B_Backup.Enabled = False
                CB_Backup.Checked = False
                CB_Backup.Enabled = False

                B_Shutdown.Enabled = False
                CB_Shutdown.Checked = False
                CB_Shutdown.Enabled = False

                B_HoloGate.Visible = True
                B_HoloGate.Enabled = True
                CB_HoloGate.Visible = True
                CB_HoloGate.Enabled = True
                CB_HoloGate.Checked = True
            Else
                B_UACActivate.Enabled = True
                CB_UACActivate.Checked = True
                CB_UACActivate.Enabled = True

                B_CopyUSB.Enabled = True
                CB_CopyUSB.Checked = True
                CB_CopyUSB.Enabled = True

                B_ExitAudit.Enabled = True
                CB_ExitAudit.Checked = True
                CB_ExitAudit.Enabled = True

                B_Backup.Enabled = True
                CB_Backup.Checked = True
                CB_Backup.Enabled = True

                B_Shutdown.Enabled = True
                CB_Shutdown.Checked = True
                CB_Shutdown.Enabled = True

                B_HoloGate.Visible = False
                B_HoloGate.Enabled = False
                CB_HoloGate.Visible = False
                CB_HoloGate.Enabled = False
                CB_HoloGate.Checked = False
            End If

            LogDialog.Go(LogString)
        Catch ex As Exception
            ErrBox.Go(ex, "Error while loading MIPP.")
            Me.Close()
        End Try

    End Sub

    Private Sub LoadOrderIDAndUser()
        User.UserRolesColl.I.Fill()
        User.UsersColl.I.Fill()
        Try
            Dim regKey As RegistryKey
            regKey = Registry.LocalMachine.OpenSubKey("Software\OEM Install", False)
            If regKey Is Nothing Then
                regKey = Registry.LocalMachine.OpenSubKey("SOFTWARE", True)
                regKey.CreateSubKey("OEM Install")
                regKey = Registry.LocalMachine.OpenSubKey("Software\OEM Install", True)
                regKey.SetValue("OrderID", "XXXXX")
                regKey.SetValue("OrderPos", "1")
                regKey.SetValue("InstallateurID", "0")
                ActiveUser = User.UsersColl.I.GetByDBID(regKey.GetValue("InstallateurID", "0"))
                User = ActiveUser
            Else
                OrderID = regKey.GetValue("OrderID", "XXXXX")
                If Not OrderID.Contains("F") Then
                    OrderID = String.Concat("F", OrderID)
                End If
                If OrderID Is Nothing OrElse Len(OrderID) < 2 Then
                    Throw New Exception("OrderID is not loaded from registry. Please use X13 before MIPP.")
                End If
                Dim OrderPCIDStr As String = regKey.GetValue("OrderPos", "0")
                If IsNumeric(OrderPCIDStr) Then
                    OrderPCID = CInt(OrderPCIDStr)
                Else
                    If OrderPCIDStr.ToLower = "rma" Then
                        OrderPCID = 0
                    Else
                        OrderPCID = -1
                    End If
                End If
                If OrderPCIDStr Is Nothing OrElse Len(OrderPCIDStr) < 1 OrElse OrderPCIDStr = "" Then
                    Throw New Exception("PCID is not loaded from registry. Please use X13 before MIPP.")
                End If
                ActiveUser = User.UsersColl.I.GetByDBID(regKey.GetValue("InstallateurID", "0"))
                User = ActiveUser
                If User Is Nothing Then
                    Throw New Exception("PCID is not loaded from registry. Please use X13 before MIPP.")
                End If


            End If
        Catch ex As Exception
            ErrBox.Go(ex)
            Me.Close()
        End Try
    End Sub

    Private Function GetDBID(OrderID As String, Transaction As SqlTransaction) As Integer
        Using cmd = Transaction.CreateCommand
            cmd.CommandText = "exec tools_db.dbo.GetOrderIDsFromID @orderID"
            cmd.Parameters.AddWithValue("@OrderID", SqlDbType.VarChar).Value = OrderID
            Return cmd.ExecuteScalar
        End Using
    End Function


    'Public Sub TestFinished()
    '    Try
    '        Dim ids As KeyValuePair(Of Integer, String) = GetDBID(OrderID)
    '        If ids.Key > 0 Then
    '            Logger.I.LogInfo(Modules.Installation, "Test wurde beendet (F" & OrderID & " - " & OrderPCID & ").", GetDBID(OrderID).Key)
    '            Dim cmd As New SqlCommand
    '            cmd.Connection = ConnectMSSQL()
    '            cmd.Parameters.AddWithValue("@OrderDBID", SqlDbType.VarChar).Value = ids.Key
    '            cmd.Parameters.AddWithValue("@OrderID", SqlDbType.VarChar).Value = ids.Value
    '            cmd.Parameters.AddWithValue("@pcorderID", SqlDbType.VarChar).Value = CInt(OrderPCID)
    '            cmd.CommandText = "Select [InstallationID] From [tools_db].[db_owner].[p_InstallationEvents] Where [OrderID] = @orderID And [PCOrderID] = @pcorderID"
    '            Dim id = cmd.ExecuteScalar
    '            If Not IsDBNull(id) And Not IsNothing(id) Then
    '                cmd.CommandText = "UPDATE [tools_db].[db_owner].[p_InstallationEvents] Set [Ended] = GETDATE() Where [InstallationID] = @id"
    '                cmd.Parameters.AddWithValue("@id", SqlDbType.BigInt).Value = CInt(id)
    '                cmd.ExecuteNonQuery()
    '            Else
    '                'InsertTest()
    '                'TestFinished()
    '            End If
    '        End If
    '    Catch ex As Exception
    '        MsgBox("Testende wurde nicht gespeichert!" & vbNewLine & ex.Message)
    '    End Try
    'End Sub


    'Public Function InsertTest() As Boolean
    '    Try
    '        Dim ids As KeyValuePair(Of Integer, String) = GetDBID(OrderID)
    '        Dim cmd As New SqlCommand
    '        cmd.Connection = ConnectMSSQL()
    '        cmd.Parameters.AddWithValue("@OrderID", SqlDbType.VarChar).Value = ids.Value
    '        cmd.Parameters.AddWithValue("@OrderDBID", SqlDbType.Int).Value = ids.Key
    '        cmd.Parameters.AddWithValue("@PCOrderID", SqlDbType.Int).Value = OrderPCID
    '        cmd.Parameters.AddWithValue("@UserID", SqlDbType.Int).Value = User.UserID
    '        cmd.Parameters.AddWithValue("@DPK", SqlDbType.VarChar).Value = GetDPKKey()
    '        cmd.CommandText = "Insert Into [tools_db].[db_owner].[p_InstallationEvents] ([OrderDBID],[OrderID],[Started],[PCOrderID],[UserID],[DPK]) VALUES (@OrderDBID, @OrderID, GETDATE(),@PCOrderID,@UserID,@DPK)"
    '        cmd.ExecuteNonQuery()
    '        Return True
    '    Catch ex As Exception
    '        MsgBox("Teststart wurde nicht gespeichert!" & vbNewLine & vbNewLine & ex.Message)
    '        Return False
    '    End Try
    'End Function

    Private Function Cleanup() As String
        Log("Desktop wird bereinigt...")
        Dim result As String = ""
        Try
            Dim DesktopPath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop)
            Dim doCleanup As Boolean = False
            If DesktopPath.Contains("Administrator") Then
                doCleanup = True
            Else
                If MsgBox("Möchten Sie alle Dateien aus """ & DesktopPath & """ löschen?", MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
                    doCleanup = True
                Else
                    doCleanup = False
                End If
            End If
            If doCleanup Then
                For Each f As String In Directory.GetFiles(DesktopPath)
                    Try
                        If Not f.Contains(Application.ExecutablePath) Then
                            RemoveReadOnly(f)
                            File.Delete(f)
                        End If
                    Catch ex As Exception
                        result = result & vbNewLine & ex.Message
                    End Try
                Next
                For Each d As String In Directory.GetDirectories(DesktopPath)
                    Try
                        DelDirectory(d, result)
                        Directory.Delete(d, True)
                    Catch ex As Exception
                        result = result & vbNewLine & ex.Message
                    End Try
                Next
            End If
            Return ""
        Catch ex As Exception
            result = result & vbNewLine & ex.Message
            Return result
        End Try
    End Function

    Private Function ExitAudit() As String
        Log("Audit Mode wird abgeschlossen...")
        Dim path As String = "C:\Windows\sysnative\sysprep\sysprep.exe"
        Try
            Dim proces As New Process()
            Dim startInfo As New ProcessStartInfo(path, "/quit /quiet /oobe")
            proces.StartInfo = startInfo
            proces.Start()
            proces.WaitForExit()
            Return proces.ExitCode.ToString
        Catch ex As Exception
            Return ex.Message & vbNewLine & path
        End Try
    End Function

    Private Function ActivateWindows() As String



        Try
            WCR(Sub(tran)
                    If IsDPKFreeToUse(tran) = False Then
                        Throw New Exception("DPK ist schon registriert. Bitte andere DPK benutzen oder Administrator kontaktieren.")
                    End If
                End Sub)
        Catch ex As Exception
            MsgBox(ex.Message)
            Return "DPK ist schon registriert!!!!!!"
        End Try




        Log("OA3 Report gestartet...")
        Dim DesktopPath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop)

        Try
            If OrderPCID.ToString = "" Then
                Throw New Exception("PCID is nothing!")
            End If
            If Len(OrderID) < 3 OrElse Len(OrderID) > 8 Then
                Throw New Exception("OrderID is in bad format!")
            End If
            Dim proc As New Process()
            Dim startInfo As New ProcessStartInfo("MIFcomOA3Tool.exe", "-report -code:werhatgebaut")
            proc.StartInfo = startInfo
            proc.StartInfo.WorkingDirectory = DesktopPath & "\X13\OA3"
            proc.Start()
            proc.WaitForExit()
            If proc.ExitCode <> 0 Then
                Throw New Exception("MIFcomOA3Tool.exe Fehler!")
            End If
            Try
                WCR(Sub(tran)
                        UpdateDPK(tran)
                        Logger.I.LogInfo(Modules.Installation, String.Concat("Windows wurde Aktiviert (Auftrag: F", OrderID.ToString, " - ", OrderPCID.ToString, ", Key: ", GetDPKKey(), ")"), 0, tran)
                    End Sub)
            Catch ex As Exception
                ErrBox.Go(ex, "UpdateDPK() Failed!")
            End Try
            Return proc.ExitCode.ToString
        Catch ex As Exception
            Return ex.Message
        End Try
    End Function

    Private Function IsDPKFreeToUse(Transaction As SqlTransaction) As Boolean
        Dim DPK As String = L_DPK.Text
        If DPK.Length < 15 Then
            Return True
        End If

        Using cmd = Transaction.CreateCommand
            Dim sqlpg As New SQLParameterGenerator
            sqlpg.Append("DPK = @DPK", "@DPK", SqlDbType.VarChar, L_DPK.Text)
            sqlpg.Append("OrderID <> @OrderID", "@OrderID", SqlDbType.VarChar, OrderID)
            sqlpg.Append("PCOrderID <> @PCOrderID", "@PCOrderID", SqlDbType.SmallInt, OrderPCID)
            cmd.CommandText = "SELECT COUNT([DPK]) FROM [tools_db].[db_owner].[p_InstallationEvents]  WHERE " & sqlpg.WhereString
            cmd.Parameters.AddRange(sqlpg.Parameters.ToArray)
            If cmd.ExecuteScalar() > 0 Then
                Return False
            Else
                Return True
            End If

        End Using

    End Function


    Public Function UpdateDPK(Transaction As SqlTransaction) As Boolean
        Try
            Dim id As Integer = GetDBID(OrderID, Transaction)
            Using cmd = Transaction.CreateCommand
                cmd.Parameters.AddWithValue("@OrderID", SqlDbType.VarChar).Value = OrderID
                cmd.Parameters.AddWithValue("@PCOrderID", SqlDbType.Int).Value = OrderPCID
                cmd.Parameters.AddWithValue("@DPK", SqlDbType.VarChar).Value = L_DPK.Text()
                cmd.CommandText = TransactionController.GetDeadlockPrioritySQL(8) & "UPDATE [tools_db].[db_owner].[p_InstallationEvents] SET [DPK] = @DPK WHERE [OrderID]=@OrderID AND [OrderPCID]=@PCOrderID"
                Dim rowsaffected As Integer = cmd.ExecuteNonQuery()
                If rowsaffected = 1 Then
                    Return True
                Else
                    Throw New Exception("Passende Events: " & rowsaffected.ToString & vbNewLine & "DPK wurde nicht gespeichert!" & vbNewLine & vbNewLine)
                End If
            End Using

        Catch ex As Exception
            Throw New Exception("Problem mit Transaktion. " & vbNewLine & " DPK wurde nicht gespeichert!" & vbNewLine & vbNewLine, ex)
            Return False
        End Try
    End Function

    Private Function UACActivate() As String
        Try
            Dim changeUACinf As New ProcessStartInfo()
            changeUACinf.FileName = "cmd.exe"
            changeUACinf.Arguments = "/C REG.EXE ADD HKLM\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\System /v EnableLUA /t REG_DWORD /d 1 /f"
            Process.Start(changeUACinf).WaitForExit()
        Catch ex As Exception
            Return ex.Message
        End Try

        Try
            Dim changeUACinf81 As New ProcessStartInfo()
            changeUACinf81.FileName = "cmd.exe"
            changeUACinf81.Arguments = "/C REG.EXE ADD HKLM\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\System /v PromptOnSecureDesktop /t REG_DWORD /d 1 /f"
            Process.Start(changeUACinf81).WaitForExit()

            changeUACinf81.Arguments = "/C REG.EXE ADD HKLM\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\System /v ConsentPromptBehaviorAdmin /t REG_DWORD /d 5 /f"
            Process.Start(changeUACinf81).WaitForExit()
        Catch ex As Exception
            Return ex.Message
        End Try
        Return "UAC im Registry aktiviert."
    End Function

    Private Function Backup() As String
        Log("Backup wird gestartet...")
        Try
            If MakeBackup2USB Then
                If ExistsHelper.FileExists("\\192.168.35.5\mdeploymentshare\Custom\MakeBackup2USB\MakeBackup2USB.exe", 4000) Then
                    File.Copy("\\192.168.35.5\mdeploymentshare\Custom\MakeBackup2USB\MakeBackup2USB.exe", "C:\Windows\Temp\MakeBackup2USB.exe")
                    File.Copy("\\192.168.35.5\mdeploymentshare\Custom\MakeBackup2USB\DriverCopyAsync.exe", "C:\Windows\Temp\DriverCopyAsync.exe")
                    Dim process2 As New Process
                    Dim startInfo2 As New ProcessStartInfo("C:\Windows\Temp\MakeBackup2USB.exe")
                    process2.StartInfo = startInfo2
                    process2.Start()
                    process2.WaitForExit()
                    Threading.Thread.Sleep(2000)
                    TryExec(Sub()
                                File.Delete("C:\Windows\Temp\MakeBackup2USB.exe")
                                File.Delete("C:\Windows\Temp\DriverCopyAsync.exe")
                            End Sub)

                    Dim ReportName As String = String.Join("_", "USB-Backup", OrderID, OrderPCID, Now.ToFileNameCompatible() & ".log").Replace(" ", "")
                    Dim DisplayName As String = String.Join(" ", "USB-Backup Report vom", Now.ToString)

                    Dim mfs = New MFSController()
                    Dim report As New FileInfo(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Desktop), "BackupReport.log"))
                    mfs.Upload(report, MFSController.MFSFileMetadata.GetForUpload(BLOBController.BLOB.DataTypes.USBBackupProtocol, ReportName, DisplayName, ActiveUser, References.ForOrderPC(Order.OrderDBID, Order.OrderID, OrderPCID)))

                    Return CStr(process2.ExitCode)
                End If
            End If

            If ExistsHelper.FileExists("C:\CustomRefresh\CustomRefresh.exe", 4000) Then
                Dim process2 As New Process
                Dim startInfo2 As New ProcessStartInfo("C:\CustomRefresh\CustomRefresh.exe", "-code:werhatgebaut")
                process2.StartInfo = startInfo2
                process2.Start()
                process2.WaitForExit()
            End If

            Dim process As New Process()
            Dim startInfo As New ProcessStartInfo("C:\Windows\sysnative\wbadmin.exe", "start backup -backupTarget:" & CB_Drive.Text.Replace("\", "") & " -include:c: -allCritical -quiet")
            startInfo.UseShellExecute = False
            startInfo.RedirectStandardError = True
            startInfo.RedirectStandardOutput = True

            AddHandler process.OutputDataReceived, AddressOf AddToLog
            AddHandler process.ErrorDataReceived, AddressOf AddToLog
            process.StartInfo = startInfo
            process.Start()
            process.BeginErrorReadLine()
            process.BeginOutputReadLine()
            process.WaitForExit()

            Return CStr(process.ExitCode)
        Catch ex As Exception
            Return ex.Message
        Finally
            ProcessingDialog.CloseForm()
        End Try
    End Function



    Public Sub Shutdown()
        Try
            Dim DesktopPath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop)

            Dim inf As New ProcessStartInfo()
            inf.Arguments = "/C choice /C Y /N /D Y /T 5 " &
              " & del """ & DesktopPath & "\*.*"" /s /q & choice /C Y /N /D Y /T 2 & rd /S /Q C:\Users\Administrator\Desktop\MIPP & del ""C:\Users\Public\Desktop\*.bat"" /s /q & shutdown -s -t 1 "
            inf.WindowStyle = ProcessWindowStyle.Hidden
            inf.CreateNoWindow = True
            inf.FileName = "cmd.exe"
            Process.Start(inf)
            Application.Exit()
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try


    End Sub

    Private Sub B_Backup_Click(sender As Object, e As EventArgs) Handles B_Backup.Click
        Dim result As String
        result = Backup()
        If result = "0" Or result = "" Then
            MsgBox("Backup erfolgreich")
        Else
            B_Backup.BackColor = Color.Red
            MsgBox(result)
        End If
    End Sub

    Private Sub B_ExitAudit_Click(sender As Object, e As EventArgs) Handles B_ExitAudit.Click
        ExitAudit()
    End Sub

    Private Sub B_Shutdown_Click(sender As Object, e As EventArgs) Handles B_Shutdown.Click
        Shutdown()
    End Sub

    Private Sub B_Start_Click(sender As Object, e As EventArgs) Handles B_Start.Click
        Dim result As String = ""

        If CB_ActivateWindows.Checked Then
            result = ActivateWindows()
            If Not result = "0" Then
                MsgBox(result)
                B_ActivateWindows.BackColor = Color.Red
                Exit Sub
            Else
                CB_ActivateWindows.Checked = False
                B_ActivateWindows.BackColor = Color.Green
            End If
        End If

        If CB_UACActivate.Checked Then
            result = UACActivate()
            If Not (result = "UAC im Registry aktiviert.") Then
                MsgBox(result)
                B_UACActivate.BackColor = Color.Red
                Exit Sub
            Else
                B_UACActivate.BackColor = Color.Green
                CB_UACActivate.Checked = False
            End If
        End If

        If CB_Cleanup.Checked Then
            result = Cleanup()
            If Not result = "" Then
                B_Cleanup.BackColor = Color.Orange
            Else
                CB_Cleanup.Checked = False
                B_Cleanup.BackColor = Color.Green
            End If
        End If

        Log(result)

        If CB_CopyUSB.Checked Then
            result = CopyUSB2MFS()
            If Not result = "" Then
                B_CopyUSB.BackColor = Color.Orange
            Else
                CB_CopyUSB.Checked = False
                B_CopyUSB.BackColor = Color.Green
            End If
        End If

        Log(result)

        If CB_ExitAudit.Checked Then
            result = ExitAudit()
            If Not (result = "" Or result = "0") Then
                MsgBox(result)
                B_ExitAudit.BackColor = Color.Red
                Exit Sub
            Else
                CB_ExitAudit.Checked = False
                B_ExitAudit.BackColor = Color.Green
            End If
        End If

        Log(result)

        If CB_Backup.Checked Then
            result = Backup()
            If Not (result = "" Or result = "0") Then
                MsgBox(result)
                B_Backup.BackColor = Color.Red
                Exit Sub
            Else
                CB_Backup.Checked = False
                B_Backup.BackColor = Color.Green
            End If
        End If

        If CB_HoloGate.Checked Then
            result = HoloGateScript()
            If Not (result = "" Or result = "0") Then
                MsgBox(result)
                B_HoloGate.BackColor = Color.Red
                Exit Sub
            Else
                CB_HoloGate.Checked = False
                B_HoloGate.BackColor = Color.Green
            End If
        End If

        Log(result)
        If CB_Shutdown.Checked Then
            If (result = "" Or result = "0") Then
                Shutdown()
            Else
                MsgBox("Wird nicht abgeschlossen, bitte Log überprüfen.")
            End If
        End If

    End Sub

    Private Sub B_Cleanup_Click(sender As Object, e As EventArgs) Handles B_Cleanup.Click
        Dim result As String = Cleanup()
        If Not result = "" Then
            B_Cleanup.BackColor = Color.Red
            MsgBox(result)
        End If
    End Sub

    Private Sub DelDirectory(path As String, ByRef errors As String)
        For Each f As String In Directory.GetFiles(path)
            Try
                If Not f = Application.ExecutablePath Then
                    RemoveReadOnly(f)
                    File.Delete(f)
                End If
            Catch ex As Exception
                errors = errors & vbNewLine & ex.Message
            End Try
        Next
        For Each d As String In Directory.GetDirectories(path)
            Try
                DelDirectory(d, errors)
            Catch ex As Exception
                Return
            End Try
        Next

    End Sub

    Private Sub RemoveReadOnly(fileName As String)
        Dim oFileInfo As New FileInfo(fileName)
        If (oFileInfo.Attributes And FileAttributes.ReadOnly) > 0 Then
            oFileInfo.Attributes = oFileInfo.Attributes Xor FileAttributes.ReadOnly
        End If
    End Sub

    Private Function GetDPKKey() As String
        Try
            Dim key As String = ""
            Dim startInfo As New ProcessStartInfo(Application.StartupPath & "\get_win8key.exe")
            Dim proc As New Process
            proc.StartInfo = startInfo
            startInfo.RedirectStandardOutput = True
            startInfo.UseShellExecute = False
            startInfo.WorkingDirectory = Application.StartupPath
            startInfo.CreateNoWindow = False
            proc.Start()
            proc.WaitForExit()
            key = proc.StandardOutput.ReadToEnd()
            If key.Contains("unexpected error") Or key.Contains("Traceback") Then
                Return ""
            Else
                Return key
            End If
        Catch ex As Exception
            MsgBox("GetDPKKey: " & ex.Message)
            Return ""
        End Try

    End Function

    Public Delegate Sub msgdel(sender As Object, args As System.Diagnostics.DataReceivedEventArgs)

    Public Sub AddToLog(sender As Object, args As System.Diagnostics.DataReceivedEventArgs)
        If Me.InvokeRequired Then
            Me.BeginInvoke(New msgdel(AddressOf AddToLog), New Object() {sender, args})
            Exit Sub
        End If
        Log(args.Data)
    End Sub

    Private Sub F_Main_FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        LogDialog.CloseForm()
    End Sub

    Sub Log(Text As String)
        LogString &= Text & vbNewLine & vbNewLine
        LogDialog.SetText(LogString)
    End Sub

    Private Sub B_UACActivate_Click(sender As Object, e As EventArgs) Handles B_UACActivate.Click
        Dim result As String = UACActivate()
        If Not result = "" Then
            Log(result)
        End If
    End Sub

    Private Sub B_CopyUSB_Click(sender As Object, e As EventArgs) Handles B_CopyUSB.Click
        Dim result As String = CopyUSB2MFS()
        If Not result = "" Then
            Log(result)
        End If
    End Sub

    Private Function HoloGateScript() As String
        If File.Exists("\\192.168.35.5\Remshare\Hologate_postSetup\Hologate-Configure.exe") Then
            Dim di As New DirectoryInfo("\\192.168.35.5\Remshare\Hologate_postSetup")
            di.CopyTo(New DirectoryInfo("C:\Hologate\Tools\Hologate_postSetup"))
        Else
            Return "Hologate script not found!"
        End If

        Process.Start("C:\Hologate\Tools\Hologate_postSetup\Hologate-Configure.exe")

        Return ""
    End Function

    Private Function CopyUSB2MFS() As String
        Dim CustomerOrderIDAndPos As String = ""
        Try
            Dim regKey As RegistryKey
            regKey = Registry.LocalMachine.OpenSubKey("Software\OEM Install", False)
            If regKey Is Nothing Then
                regKey = Registry.LocalMachine.OpenSubKey("SOFTWARE", True)
                regKey.CreateSubKey("OEM Install")
                regKey = Registry.LocalMachine.OpenSubKey("Software\OEM Install", True)
                regKey.SetValue("OrderID", "XXXXX")
                regKey.SetValue("OrderPos", "1")
            Else
                CustomerOrderIDAndPos = CStr(regKey.GetValue("OrderID", "XXXXX")) & "-" & CStr(regKey.GetValue("OrderPos", "1"))
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

        If CustomerOrderIDAndPos.StartsWith("XXXXX") Then
            Return "Auftragsnummer nicht gefunden. BIOS-Bilderspeicherung nicht möglich."
        End If


        Dim foundUSBDrive As String = ""
        Dim localDestination As String = CB_Drive.Text & "Overclocking\"
        'Dim remoteDestination As String = MIFcomToolDBSettings.BIOSScreenshotsLocation & CustomerOrderIDAndPos & "\"
        For Each drive As DriveInfo In DriveInfo.GetDrives
            Try
                If drive.DriveType = DriveType.Removable Then
                    If drive.VolumeLabel = "OC" Then
                        foundUSBDrive = drive.RootDirectory.FullName
                    Else
                        For Each File As String In My.Computer.FileSystem.GetFiles(drive.RootDirectory.FullName)
                            If File.Contains("BiosSettingsStick") OrElse File.Contains("BiosStick") Then
                                foundUSBDrive = drive.RootDirectory.FullName
                                drive.VolumeLabel = "OC"
                            End If
                        Next
                    End If

                End If
            Catch ex As Exception
            End Try
        Next

        If Not foundUSBDrive = "" Then
            Dim i As Integer = 0

            ImageFileConverter.ConvertAll(My.Computer.FileSystem.GetFiles(foundUSBDrive).Where(Function(x) x.EndsWith(".BMP")).ToList, True, Imaging.ImageFormat.Png)
            ImageFileConverter.ConvertAll(My.Computer.FileSystem.GetFiles(foundUSBDrive).Where(Function(x) x.EndsWith(".bmp")).ToList, True, Imaging.ImageFormat.Png)

            Log("Found files to upload: " & My.Computer.FileSystem.GetFiles(foundUSBDrive).Count.ToString)
            For Each file As String In My.Computer.FileSystem.GetFiles(foundUSBDrive)
                If Not file.Contains("BiosSettingsStick") AndAlso Not file.Contains("BiosStick") AndAlso Not file.EndsWith(".db") AndAlso Not file.EndsWith(".tmp") AndAlso Not file.Contains("Temp") Then
                    Try
                        My.Computer.FileSystem.CopyFile(file, localDestination & Path.GetFileName(file))
                    Catch ex As Exception
                        Log(ex.Message)
                        SendErrorMailToDeveloper(ex, "Copying UEFI Sreenshots to local C: directory failed." & Environment.NewLine & "Source: " & file & "Target: " & localDestination & Path.GetFileName(file))
                    End Try
                    Try
                        Dim mfs = New MFSController

                        Order = WCR(Function(tran)
                                        Dim ordF As New CustomerOrder.Factory
                                        Return ordF.GetOrderByID(OrderID, tran)
                                    End Function)
                        If Order Is Nothing Then
                            SendErrorMailToDeveloper(New Exception("Auftrag ist leer! BIOS-Bilderspeicherung nicht möglich."), "Problem in MIPP - BIOS fotos upload.")
                            Return "Auftrag ist leer! BIOS-Bilderspeicherung nicht möglich."
                        End If

                        Dim ref = References.ForOrderPC(Order.OrderDBID, Order.OrderID, OrderPCID)
                        Dim msff = MFSController.NewMetaDataForUpload(BLOBController.BLOB.DataTypes.BIOS, Path.GetFileName(file), "", ActiveUser, ref)
                        Dim fi = New FileInfo(file)
                        If Not mfs.FileExists(msff.RelativePath, True, True) Then
                            mfs.Upload(fi, msff)
                            Log("File " & file & " sucessfully uploaded to MFS!")
                            i += 1
                        Else
                            Log("File " & file & " already exist on MFS!")
                        End If

                    Catch ex As Exception
                        Log(ex.Message)
                    End Try
                    Try
                        My.Computer.FileSystem.DeleteFile(file)
                    Catch ex As Exception
                        Log(ex.Message)
                    End Try

                Else
                    Log(file & " skipped.")
                End If
            Next
            Log(i & " Dateien wurden vom Stick kopiert.")
        End If
        Return ""
    End Function



    <Obsolete> Private Function CopyUSB() As String
        Dim CustomerOrderIDAndPos As String = ""
        Try
            Dim regKey As RegistryKey
            regKey = Registry.LocalMachine.OpenSubKey("Software\OEM Install", False)
            If regKey Is Nothing Then
                regKey = Registry.LocalMachine.OpenSubKey("SOFTWARE", True)
                regKey.CreateSubKey("OEM Install")
                regKey = Registry.LocalMachine.OpenSubKey("Software\OEM Install", True)
                regKey.SetValue("OrderID", "XXXXX")
                regKey.SetValue("OrderPos", "1")
            Else
                CustomerOrderIDAndPos = CStr(regKey.GetValue("OrderID", "XXXXX")) & "-" & CStr(regKey.GetValue("OrderPos", "1"))
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

        If CustomerOrderIDAndPos.StartsWith("XXXXX") Then
            Return "Auftragsnummer nicht gefunden. BIOS-Bilderspeicherung nicht möglich."
        End If


        Dim foundUSBDrive As String = ""
        Dim localDestination As String = CB_Drive.Text & "Overclocking\"
        Dim remoteDestination As String = MIFcomToolDBSettings.BIOSScreenshotsLocation & CustomerOrderIDAndPos & "\"
        For Each drive As DriveInfo In DriveInfo.GetDrives
            Try
                If drive.DriveType = DriveType.Removable Then
                    For Each File As String In My.Computer.FileSystem.GetFiles(drive.RootDirectory.FullName)
                        If File.EndsWith("BiosSettingsStick") Then
                            foundUSBDrive = drive.RootDirectory.FullName
                        End If
                    Next
                End If
            Catch ex As Exception
            End Try
        Next

        If Not foundUSBDrive = "" Then
            Dim i As Integer = 0

            ImageFileConverter.ConvertAll(My.Computer.FileSystem.GetFiles(foundUSBDrive).Where(Function(x) x.EndsWith(".BMP")).ToList, True, Imaging.ImageFormat.Png)

            For Each file As String In My.Computer.FileSystem.GetFiles(foundUSBDrive)
                If Not file.EndsWith("BiosSettingsStick") Then

                    Try
                        My.Computer.FileSystem.CopyFile(file, localDestination & Path.GetFileName(file))
                    Catch ex As Exception
                        Log(ex.Message)
                    End Try
                    Try
                        My.Computer.FileSystem.CopyFile(file, remoteDestination & Path.GetFileName(file))
                    Catch ex As Exception
                        Log(ex.Message)
                    End Try
                    Try
                        My.Computer.FileSystem.DeleteFile(file)
                    Catch ex As Exception
                        Log(ex.Message)
                    End Try
                    i += 1
                End If
            Next
            For Each directory As String In My.Computer.FileSystem.GetDirectories(foundUSBDrive)
                If Not directory.EndsWith("BiosSettingsStick") Then
                    Try
                        My.Computer.FileSystem.CopyDirectory(directory, localDestination & Path.GetFileName(directory))
                    Catch ex As Exception
                        Log(ex.Message)
                    End Try
                    Try
                        My.Computer.FileSystem.CopyDirectory(directory, remoteDestination & Path.GetFileName(directory))
                    Catch ex As Exception
                        Log(ex.Message)
                    End Try
                    Try
                        My.Computer.FileSystem.DeleteDirectory(directory, FileIO.DeleteDirectoryOption.DeleteAllContents)
                    Catch ex As Exception
                        Log(ex.Message)
                    End Try
                    i += 1
                End If
            Next
            Log(i & " Dateien / Ordner wurden vom Stick kopiert.")
        End If
        Return ""
    End Function

    Private Sub B_ActivateWindows_Click(sender As Object, e As EventArgs) Handles B_ActivateWindows.Click
        Dim result As String = ActivateWindows()
        If Not result = "0" Then
            B_ActivateWindows.BackColor = Color.Red
            MsgBox(result)
        End If
    End Sub

    Private Sub B_HoloGate_Click(sender As Object, e As EventArgs) Handles B_HoloGate.Click
        Dim result As String = HoloGateScript()
        If Not result = "" Then
            B_HoloGate.BackColor = Color.Red
            MsgBox(result)
        End If
    End Sub


End Class
