﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class LogDialog
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.RTB_Log = New System.Windows.Forms.RichTextBox()
        Me.SuspendLayout()
        '
        'RTB_Log
        '
        Me.RTB_Log.Dock = System.Windows.Forms.DockStyle.Fill
        Me.RTB_Log.Location = New System.Drawing.Point(2, 2)
        Me.RTB_Log.Name = "RTB_Log"
        Me.RTB_Log.Size = New System.Drawing.Size(445, 233)
        Me.RTB_Log.TabIndex = 12
        Me.RTB_Log.Text = ""
        '
        'LogDialog
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(449, 237)
        Me.ControlBox = False
        Me.Controls.Add(Me.RTB_Log)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow
        Me.Name = "LogDialog"
        Me.Padding = New System.Windows.Forms.Padding(2)
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "Log"
        Me.TopMost = True
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents RTB_Log As System.Windows.Forms.RichTextBox
End Class
