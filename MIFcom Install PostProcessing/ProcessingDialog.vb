﻿Public Class ProcessingDialog

    Shared Property ActStyle As ProgressBarStyle
    Shared Property MaxValue As Integer
    Shared Property ActVal As Integer
    Shared Property FormStatus As FormStatEnum = FormStatEnum.Closed

    Enum FormStatEnum
        Initialising = 0
        Running = 1
        Canceled = 2
        Closing = 3
        Closed = 4
    End Enum


    Private Sub New()
        InitializeComponent()
        FormStatus = FormStatEnum.Running
        timerx.Interval = 100
        timerx.Start()
    End Sub

    Dim WithEvents timerx As New Timer

    Shared Property Dialog As ProcessingDialog

    Private Sub runnnn() Handles timerx.Tick
        Me.ProgressBar.Style = ActStyle
        Me.ProgressBar.Maximum = MaxValue

        Select Case FormStatus
            Case FormStatEnum.Running
                Select Case Me.ProgressBar.Style
                    Case ProgressBarStyle.Marquee
                        Me.L_Status.Text = "Wird bearbeitet."
                    Case ProgressBarStyle.Blocks
                        Me.ProgressBar.Increment(ActVal - Me.ProgressBar.Value)
                        Me.L_Status.Text = "Bearbeitet:  " & ActVal & " von " & MaxValue & "."
                End Select
            Case FormStatEnum.Canceled
                Me.L_Status.Text = "Vorgang wird abgebrochen."
            Case FormStatEnum.Closing
                Me.Close()
            Case FormStatEnum.Closed
                Me.Close()
        End Select

    End Sub

    Private Shared Sub Init(maxval As Integer, style As ProgressBarStyle)
        MaxValue = maxval
        ActVal = 0
        ActStyle = style
    End Sub

    Public Shared Sub Go(maxval As Integer)
        Init(maxval, ProgressBarStyle.Blocks)
        RunGo()
    End Sub

    Public Shared Sub Go()
        Init(109, ProgressBarStyle.Marquee)
        RunGo()
    End Sub

    Private Shared Sub RunGo()
        FormStatus = FormStatEnum.Initialising
        Dim t As New Threading.Thread(AddressOf Generate)
        t.Name = "ProcessingDialog"
        t.Start()
    End Sub

    Private Shared Sub Generate()
        Dim f As New ProcessingDialog()
        Dialog = f
        Application.Run(f)
    End Sub


    'Public Shared Function GetNew(i As Integer) As ProcessingDialog
    '    Dim o As New WaitingObj
    '    o.isntNothing = False
    '    o.MaxVal = i
    '    Dim t As New Threading.Thread(AddressOf Generate)
    '    t.Start(o)
    '    While o.isntNothing = False
    '        Threading.Thread.Sleep(75)
    '    End While
    '    Return o.FormObj
    'End Function

    'Private Shared Sub Generate(o As WaitingObj)
    '    o.FormObj = New ProcessingDialog(o.MaxVal)
    '    o.isntNothing = True
    'End Sub

    Public Shared Sub Increment(ByVal val As Integer)
        If ActStyle = ProgressBarStyle.Marquee Then
            ActStyle = ProgressBarStyle.Blocks
        End If
        If ActVal + val > MaxValue Then
            FormStatus = FormStatEnum.Closing
        Else
            ActStyle = ProgressBarStyle.Blocks
            ActVal += val
        End If
    End Sub

    Public Shared Sub SetMaxVal(i As Integer)
        ActStyle = ProgressBarStyle.Blocks
        MaxValue = i
    End Sub

    Private Sub B_Cancel_Click(sender As System.Object, e As System.EventArgs) Handles B_Cancel.Click
        FormStatus = FormStatEnum.Canceled
    End Sub

    Public Shared Sub CloseForm()
        FormStatus = FormStatEnum.Closing
    End Sub

    Private Sub ProcessingDialog_FormClosed(sender As System.Object, e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        FormStatus = FormStatEnum.Closed
    End Sub
End Class
