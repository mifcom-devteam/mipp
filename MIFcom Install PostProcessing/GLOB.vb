﻿Imports System.Data.SqlClient
Imports System.IO


Module GLOB
    Public ConnectionKey As String = ";o?J<!iT{B4gEv@7ghku]pC7ZFb6((_|83e3HS=3QY4m}K51sT|8AIA01*A$2&77>/42T,7BygS=3QY4m}K51sT|8AIA01*A$242T,7t5!uAfaAsK69wtJ)n!EYys,6+&7E5yy:3e-<n2kOv0&Aq?669t-.gR9981lEp9gA=X1dP>i2X8EgLNI9=2k(fS.3giF81YYhg(L6&/H4h380${"

    Public Function getWMIInfo(ByVal wmiObjectInfo As String, ByVal wmiRelativePath As String) As String
        Try
            'Give it something to report in case something wrong happens.
            getWMIInfo = "Nothing!" '

            Dim wmiClass As New System.Management.ManagementClass

            Dim wmiObject As New System.Management.ManagementObject

            wmiClass.Path.RelativePath = wmiRelativePath
            '
            'This will go through each item in the requested info. I only care about
            'the 1st for the most part but remeber that each instance could have different values.
            For Each wmiObject In wmiClass.GetInstances

                getWMIInfo = CStr(wmiObject(wmiObjectInfo))
                '
                'I only want the first instance.

                Return getWMIInfo
            Next
        Catch exc As Exception
            Return exc.Message
        End Try
    End Function


End Module
